package com.magueyetech.eurekaclient.service;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Contrat;

/**
 * Interface for contrat
 **/
public interface IContrat {

    /**
     * @return List<Contrat>
     */
    List<Contrat> findAll();

    /**
     * @param id
     * @return Contrat
     */
    Contrat get(Integer id);

    /**
     * @param contrat
     * @return Contrat
     */
    Contrat save(Contrat contrat);

    /**
     * @param contrat
     * @return Contrat
     */
    Contrat update(Contrat contrat);

    /**
     * @param id
     * @return boolean
     */
    boolean delete(Integer id);
}
