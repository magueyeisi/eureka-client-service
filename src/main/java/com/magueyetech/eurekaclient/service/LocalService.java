package com.magueyetech.eurekaclient.service;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Local;
import com.magueyetech.eurekaclient.repository.LocalRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementation for ILocal
 **/
@Service
public class LocalService implements ILocal {

    @Autowired
    private LocalRepository localRepository;

    @Override
    public List<Local> findAll() {
        return (List<Local>) localRepository.findAll();
    }

    @Override
    public Local get(Integer id) {
        return localRepository.findById(id).get();
    }

    @Override
    public Local save(Local local) {
        return localRepository.saveAndFlush(local);
    }

    @Override
    public Local update(Local local) {
        return save(local);
    }

    @Override
    public boolean delete(Integer id) {
        localRepository.deleteById(id);
        return !localRepository.existsById(id);
    }

}
