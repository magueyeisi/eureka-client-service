package com.magueyetech.eurekaclient.service;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Contrat;
import com.magueyetech.eurekaclient.repository.ContratRepository;
import com.magueyetech.eurekaclient.repository.LocalRepository;
import com.magueyetech.eurekaclient.repository.LocataireRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementation for IContrat
 **/
@Service
public class ContratService implements IContrat {

    @Autowired
    private ContratRepository contratRepository;

    @Autowired
    private LocalRepository localRepository;

    @Autowired
    private LocataireRepository locataireRepository;

    @Override
    public List<Contrat> findAll() {
        return (List<Contrat>) contratRepository.findAll();
    }

    @Override
    public Contrat get(Integer id) {
        return contratRepository.findById(id).get();
    }

    @Override
    public Contrat save(Contrat contrat) {
        contrat.setLocal(localRepository.findById(contrat.getLocal().getId()).get());
        contrat.setLocataire(locataireRepository.findById(contrat.getLocataire().getId()).get());
        return contratRepository.saveAndFlush(contrat);
    }

    @Override
    public Contrat update(Contrat contrat) {
        return save(contrat);
    }

    @Override
    public boolean delete(Integer id) {
        contratRepository.deleteById(id);
        return !contratRepository.existsById(id);
    }

}
