package com.magueyetech.eurekaclient.service;

import java.util.Arrays;
import java.util.List;

import com.magueyetech.eurekaclient.domain.Locataire;
import com.magueyetech.eurekaclient.repository.LocataireRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.*;

/**
 * Service implementation for ILocataire
 **/
@Service
public class LocataireService implements ILocataire {

    @Value("${server.port}")
    private String PORT;

    @Value("${locataires-api-host}")
    private String LOCATAIRES_SERVICE_HOST;

    @Autowired
    private LocataireRepository locataireRepository;

    @Override
    public List<Locataire> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        String url = String.format("%s%s", LOCATAIRES_SERVICE_HOST, "/api/locataires");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Locataire[]> locataires = new HttpEntity<Locataire[]>(headers);
        ResponseEntity<Locataire[]> response = restTemplate.exchange(url, HttpMethod.GET, locataires,
                Locataire[].class);
        HttpStatus statusCode = response.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            Locataire[] list = response.getBody();
            return Arrays.asList(list);
        }
        return null;
        // return (List<Locataire>) locataireRepository.findAll();
    }

    @Override
    public Locataire get(Integer id) {
        return locataireRepository.findById(id).get();
    }

    @Override
    public Locataire save(Locataire locataire) {
        return locataireRepository.saveAndFlush(locataire);
    }

    @Override
    public Locataire update(Locataire locataire) {
        return save(locataire);
    }

    @Override
    public boolean delete(Integer id) {
        locataireRepository.deleteById(id);
        return !locataireRepository.existsById(id);
    }

}
