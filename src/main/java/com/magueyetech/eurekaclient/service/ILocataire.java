package com.magueyetech.eurekaclient.service;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Locataire;

/**
 * Interface for contrat
 **/
public interface ILocataire {

    /**
     * @return List<Locataire>
     */
    List<Locataire> findAll();

    /**
     * @param id
     * @return Locataire
     */
    Locataire get(Integer id);

    /**
     * @param locataire
     * @return Locataire
     */
    Locataire save(Locataire locataire);

    /**
     * @param locataire
     * @return Locataire
     */
    Locataire update(Locataire locataire);

    /**
     * @param id
     * @return boolean
     */
    boolean delete(Integer id);
}
