package com.magueyetech.eurekaclient.service;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Local;

/**
 * Interface for contrat
 **/
public interface ILocal {

    /**
     * @return List<Local>
     */
    List<Local> findAll();

    /**
     * @param id
     * @return Local
     */
    Local get(Integer id);

    /**
     * @param local
     * @return Local
     */
    Local save(Local local);

    /**
     * @param local
     * @return Local
     */
    Local update(Local local);

    /**
     * @param id
     * @return boolean
     */
    boolean delete(Integer id);
}
