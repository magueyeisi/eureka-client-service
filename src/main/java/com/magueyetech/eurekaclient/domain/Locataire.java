package com.magueyetech.eurekaclient.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents a locataire.
 *
 * @author Papa Magueye GUEYE - MagueyeTech
 */
@Entity
@Table(name = "locataires")
public class Locataire {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String prenom;
    private String nom;
    private String adresse;
    private String telephone;
    private String cin;
    private String email;
    private String datenaiss;

    @JsonIgnoreProperties
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "locataire")
    private List<Contrat> contrats;

    /**
     * @return Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return String
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return String
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return String
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return String
     */
    public String getCin() {
        return cin;
    }

    /**
     * @param cin
     */
    public void setCin(String cin) {
        this.cin = cin;
    }

    /**
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return String
     */
    public String getDatenaiss() {
        return datenaiss;
    }

    /**
     * @param datenaiss
     */
    public void setDatenaiss(String datenaiss) {
        this.datenaiss = datenaiss;
    }
}
