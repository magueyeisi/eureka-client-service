package com.magueyetech.eurekaclient.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents a local.
 *
 * @author Papa Magueye GUEYE - MagueyeTech
 */
@Entity
@Table(name = "locaux")
public class Local {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String numero;
    private String nom;
    private String position;
    private boolean occupe;

    /**
     * @return Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return String
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return String
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return boolean
     */
    public boolean getOccupe() {
        return occupe;
    }

    /**
     * @param occupe
     */
    public void setOccupe(boolean occupe) {
        this.occupe = occupe;
    }
}
