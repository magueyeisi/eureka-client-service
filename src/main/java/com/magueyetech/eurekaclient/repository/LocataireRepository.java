package com.magueyetech.eurekaclient.repository;

import com.magueyetech.eurekaclient.domain.Locataire;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LocataireRepository extends JpaRepository<Locataire, Integer> {

}
