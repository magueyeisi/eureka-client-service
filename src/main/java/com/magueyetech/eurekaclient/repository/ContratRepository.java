package com.magueyetech.eurekaclient.repository;

import com.magueyetech.eurekaclient.domain.Contrat;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ContratRepository extends JpaRepository<Contrat, Integer> {

}
