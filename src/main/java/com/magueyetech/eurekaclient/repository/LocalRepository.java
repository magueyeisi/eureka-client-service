package com.magueyetech.eurekaclient.repository;

import com.magueyetech.eurekaclient.domain.Local;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalRepository extends JpaRepository<Local, Integer> {

}
