package com.magueyetech.eurekaclient.controller;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Contrat;
import com.magueyetech.eurekaclient.service.ContratService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/contrats")
public class ContratController {
    @Autowired
    private ContratService contratService;

    @GetMapping("")
    public List<Contrat> getContrats() {
        return contratService.findAll();
    }

    @GetMapping("/{id}")
    public Contrat getContrat(@PathVariable Integer id) {
        return contratService.get(id);
    }

    @PostMapping("")
    public Contrat saveContrat(@RequestBody Contrat contrat) {
        System.out.println(contrat);
        // contrat.setLocal(contrat.getLocal());
        return contratService.save(contrat);
    }

    @PutMapping("")
    public Contrat updateContrat(@RequestBody Contrat contrat) {
        return contratService.update(contrat);
    }

    @DeleteMapping("/{id}")
    public boolean deleteContrat(@PathVariable Integer id) {
        return contratService.delete(id);
    }
}
