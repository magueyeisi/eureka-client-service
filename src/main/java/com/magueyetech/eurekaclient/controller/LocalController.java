package com.magueyetech.eurekaclient.controller;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Local;
import com.magueyetech.eurekaclient.service.LocalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(path = "/api")
public class LocalController {
    @Autowired
    private LocalService localService;

    @GetMapping("/locaux")
    public List<Local> getLocals() {
        return localService.findAll();
    }

    @GetMapping("/locaux/{id}")
    public Local getLocal(@PathVariable Integer id) {
        return localService.get(id);
    }

    @PostMapping("/locaux")
    public Local saveLocal(@RequestBody Local local) {
        return localService.save(local);
    }

    @PutMapping("/locaux")
    public Local updateLocal(@RequestBody Local local) {
        return localService.update(local);
    }

    @DeleteMapping("/locaux/{id}")
    public boolean updateLocal(@PathVariable Integer id) {
        return localService.delete(id);
    }
}
