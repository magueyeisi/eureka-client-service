package com.magueyetech.eurekaclient.controller;

import java.util.List;

import com.magueyetech.eurekaclient.domain.Locataire;
import com.magueyetech.eurekaclient.service.LocataireService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(path = "/api")
public class LocataireController {
    @Autowired
    private LocataireService locataireService;

    @GetMapping("/locataires")
    public List<Locataire> getLocataires() {
        return locataireService.findAll();
    }

    @GetMapping("/locataires/{id}")
    public Locataire getLocataire(@PathVariable Integer id) {
        return locataireService.get(id);
    }

    @PostMapping("/locataires")
    public Locataire saveLocataire(@RequestBody Locataire locataire) {
        return locataireService.save(locataire);
    }

    @PutMapping("/locataires")
    public Locataire updateLocataire(@RequestBody Locataire locataire) {
        return locataireService.update(locataire);
    }

    @DeleteMapping("/locataires/{id}")
    public boolean updateLocataire(@PathVariable Integer id) {
        return locataireService.delete(id);
    }
}
