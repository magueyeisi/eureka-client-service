package com.magueyetech.eurekaclient.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Date;

import com.magueyetech.eurekaclient.controller.ContratController;
import com.magueyetech.eurekaclient.repository.ContratRepository;
import com.magueyetech.eurekaclient.service.ContratService;
import com.magueyetech.eurekaclient.service.LocalService;
import com.magueyetech.eurekaclient.service.LocataireService;

public class ContratTest {

    @Autowired
    ContratService contratService;

    @Autowired
    LocataireService locataireService;

    @Autowired
    LocalService localService;

    @InjectMocks
    private ContratController contratController;

    @Mock
    private ContratRepository contratRepository;

    private Contrat contrat;
    private Locataire locataire;
    private Local local;

    @BeforeEach
    public void init() {
        local = localService.get(4);
        locataire = locataireService.get(2);

        contrat = new Contrat();
        contrat.setNumero("C00090");
        contrat.setMontantLoyer((float) 70000);
        contrat.setStatus("cours");
        contrat.setDate(java.sql.Date.valueOf(java.time.LocalDate.now()));
        contrat.setDebut(java.sql.Date.valueOf(java.time.LocalDate.now()));
        contrat.setDuree(12);
    }

    @Test
    @DisplayName("Contrat should be inserted")
    public void insertTest() throws Exception {

        contrat = contratService.save(contrat);
        assertThat(contrat.getLocataire()).isEqualTo(locataire);
        assertThat(contrat.getLocal()).isEqualTo(local);
    }

    @Test
    public void testGetContratById() {
        Contrat C = new Contrat();
        C.setId(1);
        when(contratService.get(1)).thenReturn(C);

        Contrat contrat = contratController.getContrat(1);

        verify(contratRepository).findById(1);

        assertEquals(1, contrat.getId(), "Contrat id should wordx");
        assertThat(contrat.getId()).isEqualTo(1);
    }

}
