package com.magueyetech.eurekaclient.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.magueyetech.eurekaclient.controller.LocalController;
import com.magueyetech.eurekaclient.repository.LocalRepository;
import com.magueyetech.eurekaclient.service.LocalService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;

public class LocalTest {

    private Local local;

    @MockBean
    LocalService localService;

    @InjectMocks
    private LocalController localController;

    @Mock
    private LocalRepository localRepository;

    @BeforeEach
    public void init() {
        local = new Local();
        local.setNom("Appartement 1");
        local.setNumero("1");
        local.setOccupe(false);
        local.setPosition("premier");
    }

    @Test
    @DisplayName("Local object should work")
    public void testLocal() {
        assertEquals("Appartement 1", local.getNom(), "Local Nom should work");
        assertEquals("1", local.getNumero(), "Local Numero should work");
        assertEquals(false, local.getOccupe(), "Local Occupe should work");
        assertEquals("premier", local.getPosition(), "Local Position should work");
    }

    @Test
    public void testGetContratById() {
        Local C = new Local();
        C.setId(4);
        when(this.localService.get(4)).thenReturn(C);

        Local local = localController.getLocal(4);

        verify(localRepository).findById(4);

        assertEquals(4, local.getId(), "Local id should wordx");
        assertThat(local.getId()).isEqualTo(4);
    }
}
