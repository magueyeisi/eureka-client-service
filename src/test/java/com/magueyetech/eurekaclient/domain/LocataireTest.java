package com.magueyetech.eurekaclient.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.mysql.cj.util.TestUtils;

public class LocataireTest {

    private Locataire locataire;

    @BeforeEach
    public void init() {
        locataire = new Locataire();
        locataire.setPrenom("Papa Magueye");
        locataire.setNom("GUEYE");
        locataire.setEmail("magueyetech@gmail.com");
        locataire.setTelephone("776707316");
        locataire.setCin("1619199300146");
        locataire.setAdresse("Dakar Plateau");
        locataire.setDatenaiss("1992-12-29");
    }

    @Test
    @DisplayName("Locataire object should work")
    public void testLocataire() {
        assertEquals("Papa Magueye", locataire.getPrenom(), "Locataire Prenom should work");
        assertEquals("GUEYE", locataire.getNom(), "Locataire Nom should work");
        assertEquals("magueyetech@gmail.com", locataire.getEmail(), "Locataire Email should work");
        assertEquals("776707316", locataire.getTelephone(), "Locataire Telephone should work");
        assertEquals("1619199300146", locataire.getCin(), "Locataire Cin should work");
        assertEquals("Dakar Plateau", locataire.getAdresse(), "Locataire Adresse should work");
        assertEquals("1992-12-29", locataire.getDatenaiss(), "Locataire Datenaiss should work");
    }
}
